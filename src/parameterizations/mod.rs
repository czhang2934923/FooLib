use anyhow::Result;
/// This is a linker file for telling Rust how to find modules.
/// In here we can add common things shared by submodules (foo).
pub mod foo;

/// An interface to be shared between all "things" that
/// offer parameterizations.
/// Below are some possible functions we may care about.

/// NOTE: There are some things not great to put in this interface:
///        - Generic calculate() functions can have wildly varying numbers
///          of parameters and using a vector to encompass them instead of
///          giving concrete names is not ideal.
pub trait Parameterize: Sized {
    // FUTURE: Maybe add some functionality to Serialize/Deserialize
    //       from a file?
    // fn new_from_file()

    // Maybe we want to be able to see stats
    fn display(&self);

    /// A generic function for initializing a paramterization.
    /// While the input values might not be descriptive, as long
    /// as the order is right, we can pattern match on them.
    fn initialize(constants: Vec<f64>) -> Result<Self>;

    /// A generic function for calculating outputs from a parameterization.
    /// While the input values might not be descriptive, as long
    /// as the order is right, we can pattern match on them.
    fn calculate(&self, inputs: Vec<f64>) -> Result<Vec<f64>>;
}
