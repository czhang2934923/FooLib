use clap::Parser;
use foolib::parameterizations::{foo::FooParameterization, Parameterize};
use tracing::warn;

use crate::args::CommandLineArgs;
use anyhow::Result;
use std::io;

pub mod args;

/// The main function for our binary.
fn main() -> Result<()> {
    let cli_args = CommandLineArgs::parse();

    // If not interactive mode, calculate and spit out numbers.
    // This is nice if we want to make it scriptable.
    if !cli_args.interactive {
        match cli_args.parameterization.unwrap().as_str() {
            // FUTURE: Use a simple factory method so the matching
            //         of string -> parameter object can be pulled out.
            "foo" => {
                let foo_param = FooParameterization::initialize(cli_args.constants.unwrap())?;
                println!(
                    "Result: {:?}",
                    foo_param.calculate(cli_args.inputs.unwrap())?
                );
                Ok(())
            }

            "none" => {
                warn!("No parameterization specified. Exiting.");
                Ok(())
            }
            _ => {
                warn!("Unsupported parameterization. Exiting.");
                Ok(())
            }
        }
    } else {
        println!("Entering interactive mode. Type 'exit' to exit.");
        loop {
            // Read input from stdin
            let stdin = io::stdin();

            println!("Enter a parameterization (foo, etc.):");
            let mut param = String::new();
            stdin.read_line(&mut param)?;
            let param = param.trim();

            if param == "exit" {
                return Ok(());
            }

            println!("Enter the constants for the parameterization (1 3.4 9 ...):");
            let mut str_consts = String::new();
            stdin.read_line(&mut str_consts)?;

            println!("Enter the inputs for the parameterization (1 4 4 ...):");
            let mut str_inputs = String::new();
            stdin.read_line(&mut str_inputs)?;

            // Parse the input strings into a vector of f64 values
            let constants: Result<Vec<f64>> = str_consts
                .split_whitespace()
                .map(|s| Ok(s.trim().parse::<f64>()?))
                .collect();

            let inputs: Result<Vec<f64>> = str_inputs
                .split_whitespace()
                .map(|s| Ok(s.trim().parse::<f64>()?))
                .collect();

            // FUTURE: Make this more generic, maybe using a factory method to
            //         spit out a parameterization.
            match param {
                "foo" => {
                    let foo_param = FooParameterization::initialize(constants?)?;
                    println!("Result: {:?}", foo_param.calculate(inputs?)?);
                }
                _ => {
                    println!("Unsupported Parameterization '{}'.", param);
                    continue;
                }
            }
        }
    }
}
