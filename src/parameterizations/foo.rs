use super::Parameterize;
use anyhow::{anyhow, Result};
use std::f64::consts::PI;

/// The main object for a foo parameterization.

/// I opted for this instead of a generic struct because
/// parameterizations can differ greatly, similar to
/// distributions and their varying constants/parameters.

#[derive(Debug)]
pub struct FooParameterization {
    // We can define constants to keep track of and other things here
    pi: f64,
}

// Implement the generic interface
impl Parameterize for FooParameterization {
    // Passing pi in to show what it'd be like for other
    // parameterizations with actual variable constants
    fn initialize(constants: Vec<f64>) -> Result<Self> {
        match constants[..] {
            [pi] => Ok(FooParameterization { pi }),

            _ => Err(anyhow!(
                "Expected 1 constant (pi), received {}.",
                constants.len()
            )),
        }
    }
    // Could make this return a string if we wanted more than a stdout printing
    fn display(&self) {
        println!("{:?}", self);
    }

    fn calculate(&self, inputs: Vec<f64>) -> Result<Vec<f64>> {
        match inputs[..] {
            [radius] => {
                if radius < 0.0 {
                    return Err(anyhow!("radius must be a positive number."));
                }

                // TODO: Should also check for overflow
                Ok(vec![(4.0 / 3.0) * self.pi * radius.powi(3)])
            }

            _ => Err(anyhow!(
                "Expected 1 input (radius), received {}.",
                inputs.len()
            )),
        }
    }
}

impl Default for FooParameterization {
    fn default() -> Self {
        FooParameterization { pi: PI }
    }
}

impl FooParameterization {}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    // Test negative radius is an error
    fn negative_radius() {
        let foo = FooParameterization::default();
        assert!(matches!(foo.calculate(vec![-1.0]), Err(_)));
    }
}
