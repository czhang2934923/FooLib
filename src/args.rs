/// Provides structures using the clap library to read in
/// any arguments from the command line.
use clap::Parser;

/// Arguments from the command line.
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
pub struct CommandLineArgs {
    /// Whether to run in interactive mode
    #[arg(short, long, default_value_t = false)]
    pub interactive: bool,

    /// The parameterization to run.
    /// Valid inputs are ("foo")
    #[arg(short, long, required_unless_present = "interactive")]
    pub parameterization: Option<String>,

    /// The input arguments to the parameterization, separated by spaces
    #[arg(short = 'x', long, required_unless_present = "interactive")]
    pub inputs: Option<Vec<f64>>,

    /// The constants of the parameterization, separated by spaces
    #[arg(short, long, required_unless_present = "interactive")]
    pub constants: Option<Vec<f64>>,
}
